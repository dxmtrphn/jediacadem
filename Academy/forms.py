from django import forms

from Academy.models import Candidate, Test


class CandidateCreateForm(forms.ModelForm):
    class Meta:
        model = Candidate
        fields = ['name', 'age', 'email', 'planet']


# class TestForm(forms.ModelForm):
#     class Meta:
#         model = Test
#         fields = ['questions']