
from django.urls import path

from Academy import views

urlpatterns = [
    path('jedi', views.jedi_choice, name='jedi_choice'),
    path('jedi/<int:id>', views.jedi, name='jedi'),
    path('candidate', views.candidate_register, name='candidate_register'),
    path('candidate/<int:id>', views.candidate, name='candidate'),
    path('test', views.test, name='test'),
    path('', views.main, name='index')
]
