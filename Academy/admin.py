from django.contrib import admin

# Register your models here.
from Academy.models import Planet, Jedi, Test, Question


@admin.register(Planet)
class PlanetAdmin(admin.ModelAdmin):
    pass

@admin.register(Jedi)
class JediAdmin(admin.ModelAdmin):
    pass

@admin.register(Test)
class TestAdmin(admin.ModelAdmin):
    pass

@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    pass