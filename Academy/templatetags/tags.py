from django import template
from django.db.models import Count

from Academy.models import Candidate, Jedi

register = template.Library()

@register.simple_tag
def padawan_count(jedi_id):
    jedi_id = int(jedi_id)
    padawans = Candidate.objects.filter(teacher=Jedi.objects.get(id=jedi_id))
    return len(padawans)