from django.contrib import messages
from django.core.mail import send_mail
from django.db import IntegrityError
from django.db.models import Count
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.template.context_processors import csrf
from Academy.forms import CandidateCreateForm
from Academy.models import Candidate, Test, Answer, Question, Jedi, Planet
from JediAcademy import settings


def main(request):
    return render(request, 'index.html')


def candidate_register(request):
    args = {}
    args['form'] = CandidateCreateForm()
    if request.POST:
        args.update(csrf(request))
        candidate = Candidate()
        form = CandidateCreateForm(request.POST, instance=candidate)
        args['form'] = form
        if form.is_valid():
            candidate = None
            if (int(request.POST['age'])>=1 and int(request.POST['age'])<=160):
                candidate = form.save()
                request.session['candidate'] = candidate
                return HttpResponseRedirect('test')
            else:
                messages.add_message(request, messages.ERROR, 'Возраст долже быть в пределах от 1 до 160 лет включительно.')
                return render(request, 'candidate_registration.html', args)
        else:
            messages.add_message(request, messages.ERROR, 'Этот email уже занят.')
            return render(request, 'candidate_registration.html', args)
    else:
        return render(request, 'candidate_registration.html', args)

def jedi_choice(request):
    args = {}
    # список джедаев, у которых больше одного подавана
    # args['jedis'] = Jedi.objects.annotate(num_candidate=Count('candidate')).filter(num_candidate__gt=1)
    # список всех джедаев
    args['jedis'] = Jedi.objects.all()
    return render(request, 'jedi_choice.html', args)


def test(request):
    args = {}
    args['form'] = CandidateCreateForm()
    try:
        candidate = request.session['candidate']
        if request.POST:
            for question in Test.objects.all()[0].questions.all():
                answer = Answer(answer=(request.POST[str(question.id)]))
                answer.save()
                answer.question.add(Question.objects.get(id=question.id))
                answer.save()
                candidate.answers.add(answer)
                candidate.save()
            del request.session['candidate']
            messages.add_message(request, messages.ERROR, 'Вы успешно зарегистрировали свою кандидатуру. Ждите результатов.')
            return render(request, 'candidate_registration.html', args)
        else:
            if (Test.objects.all()):
                args['test'] = Test.objects.all()[0]
                return render(request, 'test.html', args)
            else:
                messages.add_message(request, messages.ERROR, 'Тест пока еще не создан.')
                return render(request, 'test.html', args)
    except KeyError:
        messages.add_message(request, messages.ERROR, 'Вы должны сначала зарегистрировать свою кандидатуру.')
        return render(request, 'candidate_registration.html', args)
def jedi(request, id):
    args = {}
    args['candidates'] = Candidate.objects.filter(planet=Jedi.objects.get(id=id).planet, teacher=None).exclude(answers__isnull=True)
    args['jedi_id'] = id
    return render(request, 'jedi.html', args)


def candidate(request, id):
    args = {}
    candidate = Candidate.objects.get(id=id)
    args['candidate'] = candidate
    if request.POST:
        args['candidates'] = Candidate.objects.filter(planet=Jedi.objects.get(id=request.GET['jedi_id']).planet, teacher=None).exclude(answers__isnull=True)
        args['jedi_id'] = request.GET['jedi_id']
        if (len(Candidate.objects.filter(teacher=Jedi.objects.get(id=request.GET['jedi_id'])))<3):
            candidate.teacher = Jedi.objects.get(id=request.GET['jedi_id'])
            candidate.save()
            send_mail('Вас приняли в подаваны!', 'Вы были приняты к обучению у джедая %s. Приятного обучения!' % candidate.teacher.name, settings.EMAIL_HOST_USER,
                  [candidate.email], fail_silently=False)
            return render(request, 'jedi.html', args)
        else:
            messages.add_message(request, messages.ERROR, 'Вы уже превысили допустимое количество(3) подаванов.')
            return render(request, 'candidate.html', args)
    else:
        return render(request, 'candidate.html', args)