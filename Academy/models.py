from django.db import models


class Planet(models.Model):
    name = models.CharField(max_length=150 , unique=True, blank=False, default="Earth")

    def __str__(self):
        return '%s' % self.name

class Question(models.Model):
    question = models.TextField(max_length=1000 , unique=True, blank=False, null=False)
    answer = models.BooleanField(default=False)
    # answer = models.OneToOneField('Answer', on_delete=models.CASCADE)

    def __str__(self):
        return '%s' % self.question

class Answer(models.Model):
    answer = models.BooleanField(default=False)
    question = models.ManyToManyField('Question', related_name="question_of")



class Jedi(models.Model):
    name = models.CharField(max_length=100, blank=False, null=False)
    planet = models.ForeignKey('Planet', on_delete=models.PROTECT, null=False, blank=False)
    # padawan = models.ManyToManyField('Candidate', related_name='padawan_of', null=True, blank=True)

    def __str__(self):
        return '%s' % self.name

class Candidate(models.Model):
    name = models.CharField(max_length=100, blank=False, null=False)
    planet = models.ForeignKey('Planet', on_delete=models.PROTECT, related_name='planet_of')
    age = models.IntegerField(blank=False, null=False)
    email = models.EmailField(blank=False, null=False, unique=True)
    answers = models.ManyToManyField('Answer', related_name='answers_of')
    teacher = models.ForeignKey('Jedi', on_delete=models.PROTECT, null=True, blank=True)

    def __str__(self):
        return '%s' % self.name

class Test(models.Model):
    code = models.IntegerField(unique=True)
    questions = models.ManyToManyField('Question')

    def __str__(self):
        return '%s' % self.code